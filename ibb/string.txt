	
函数名	描述
去空格或或其他字符:	
trim()	删除字符串两端的空格或其他预定义字符
rtrim()	删除字符串右边的空格或其他预定义字符
chop()	rtrim()的别名
ltrim()	删除字符串左边的空格或其他预定义字符
dirname()	返回路径中的目录部分
字符串生成与转化:	
str_pad()	把字符串填充为指定的长度
str_repeat()	重复使用指定字符串
str_split()	把字符串分割到数组中
strrev()	反转字符串
wordwrap()	按照指定长度对字符串进行折行处理
str_shuffle()	随机地打乱字符串中所有字符
parse_str()	将字符串解析成变量
number_format()	通过千位分组来格式化数字
大小写转换:	
strtolower()	字符串转为小写
strtoupper()	字符串转为大写
ucfirst()	字符串首字母大写
ucwords()	字符串每个单词首字符转为大写
html标签关联:	
htmlentities()	把字符转为HTML实体
htmlspecialchars()	预定义字符转html编码
nl2br()	\n转义为<br>标签
strip_tags()	剥去 HTML、XML 以及 PHP 的标签
addcslashes()	在指定的字符前添加反斜线转义字符串中字符
stripcslashes()	 删除由addcslashes()添加的反斜线
addslashes()	指定预定义字符前添加反斜线
stripslashes()	删除由addslashes()添加的转义字符
quotemeta()	在字符串中某些预定义的字符前添加反斜线
chr()	从指定的 ASCII 值返回字符
ord()	返回字符串第一个字符的 ASCII 值
字符串比较:	
strcasecmp()	不区分大小写比较两字符串
strcmp()	区分大小写比较两字符串
strncmp()	比较字符串前n个字符,区分大小写
strncasecmp()	比较字符串前n个字符,不区分大小写
strnatcmp()	自然顺序法比较字符串长度,区分大小写
strnatcasecmp()	自然顺序法比较字符串长度,不区分大小写
字符串切割与拼接:	
chunk_split()	将字符串分成小块
strtok()	切开字符串
explode()	使用一个字符串为标志分割另一个字符串
implode()	同join,将数组值用预订字符连接成字符串
substr()	截取字符串
字符串查找替换:	
str_replace()	字符串替换操作,区分大小写
str_ireplace()	字符串替换操作,不区分大小写
substr_count()	统计一个字符串,在另一个字符串中出现次数
substr_replace()	替换字符串中某串为另一个字符串
similar_text()	返回两字符串相同字符的数量
strrchr()	返回一个字符串在另一个字符串中最后一次出现位置开始到末尾的字符串
strstr()	返回一个字符串在另一个字符串中开始位置到结束的字符串
strchr()	strstr()的别名,返回一个字符串在另一个字符串中首次出现的位置开始到末尾的字符串
stristr()	返回一个字符串在另一个字符串中开始位置到结束的字符串，不区分大小写
strtr()	转换字符串中的某些字符
strpos()	寻找字符串中某字符最先出现的位置
stripos()	寻找字符串中某字符最先出现的位置,不区分大小写
strrpos()	寻找某字符串中某字符最后出现的位置
strripos()	寻找某字符串中某字符最后出现的位置,不区分大小写
strspn()	返回字符串中首次符合mask的子字符串长度
strcspn()	返回字符串中不符合mask的字符串的长度
字符串统计:	
str_word_count()	统计字符串含有的单词数
strlen()	统计字符串长度
count_chars()	统计字符串中所有字母出现次数(0..255)
字符串编码:	
md5()	字符串md5编码